# Assignment: Threads and processes

## Task 1: Threads (`threads.c`)

Write a program that created N threads. Each thread should increment shared variable A M times. Parameters N and M should be passed as command line arguemnts. Main Thread sets value of A to 0 and then creates N threads. When all threads are finished main thread prints value of variable A.

## Task 2: Synchronisation (`lamport_processes.c` and `dekker_threads.c`)

Write 2 programs:
- `lamport_processes.c` - Synchronize N processes using Lamport algorithm.
- `dekker_threads.c` - Synchronize 2 threads using Dekker algorithm.
