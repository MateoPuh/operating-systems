#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <signal.h>

int TRAZIMID;
int BROJID;
int STOLID;
int *TRAZIM;
int *BROJ;
int *STOL;

int M;
int N;

void udi_u_kriticni_odsjecak(int i)
{
	int max = BROJ[0];
	TRAZIM[i] = 1;

	for (int j = 1; j < N; j++) {
		if (BROJ[j] > max) {
			max = BROJ[j];
		}
	}

	BROJ[i] = max + 1;

	sleep(1);

	TRAZIM[i] = 0;

	for (int j = 0; j < N; j++) {
		while (TRAZIM[j] != 0);

		while (BROJ[j] != 0 && (BROJ[j] < BROJ[i] ||
			 (BROJ[j] == BROJ[i] && j < i)));
	}
}

void izadi_iz_kriticnog_odsjecka(int i)
{
	BROJ[i] = 0;
}

void ispisi_stanje()
{
	for (int i = 0; i < M; i++) {
		if(STOL[i] == 0) {
			printf( "- " );
		} else {
			printf( "%d ", STOL[i]);
		}
	}
	printf( "\n" );
}

void process_func(int i)
{
	int brojStola;
	int slobodni;

	while(1) {
		slobodni = 0;
		for (int j = 0; j < M; j++) {
			if (STOL[j] == 0) {
				slobodni ++;
			}
		}

		if (slobodni == 0) {
			break;
		}

		srand((unsigned) time(NULL) + i);

		brojStola = rand() % M ;
		printf( "Proces %d: odabirem stol %d\n",
			 i + 1, brojStola + 1);

		udi_u_kriticni_odsjecak(i);

		if (STOL[brojStola] != 0) {
			printf( "Proces %d: neuspjela rezervacija stola %d, stanje:\n",
				 i+1, brojStola + 1);
			ispisi_stanje();
		} else {
			STOL[brojStola] = i+1;
			printf( "Proces %d: rezervacija stola %d, stanje:\n",
				 i+1, brojStola + 1);
			ispisi_stanje();
		}

		izadi_iz_kriticnog_odsjecka(i);

		sleep(1);
	}
}

void brisi()
{
	shmdt((char *) TRAZIM);
	shmdt((char *) BROJ);
	shmdt((char *) STOL);
	shmctl(TRAZIMID, IPC_RMID, NULL);
	shmctl(BROJID, IPC_RMID, NULL);
	shmctl(STOLID, IPC_RMID, NULL);
	exit(0);
}

int main(int argc, char* argv[])
{
	if (argc == 3) {
		N = atoi(argv[1]);
		M = atoi(argv[2]);
	} else {
		printf( "Error: Wrong number of cammand line arguments.\n" );
		exit(1);
	}

	TRAZIMID = shmget(IPC_PRIVATE, N * sizeof(int), 0600);
	BROJID = shmget(IPC_PRIVATE, N * sizeof(int), 0600);
	STOLID = shmget(IPC_PRIVATE, M * sizeof(int), 0600);

	if (TRAZIMID == -1 || BROJID == -1 || STOLID == -1) {
		printf( "Error: Cannot make shared memory.\n" );
		exit(1);
	}

	TRAZIM = (int *) shmat(TRAZIMID, NULL, 0);
	BROJ = (int *) shmat(BROJID, NULL, 0);
	STOL = (int *) shmat(STOLID, NULL, 0);

	for (int i = 0; i < N; i++) {
		TRAZIM[i] = 0;
		BROJ[i] = 0;
	}

	for (int i = 0; i < M; i++) {
		STOL[i] = 0;
	}

	for (int i = 0; i < N; i++) {
		switch (fork()){
		case -1:
			printf("Error: Cannot make process.\n");
			exit(0);
		case 0:
			process_func(i);
			exit(0);
		default:
			break;
		}
	}

	sigset(SIGINT, brisi);

	for (int i = 0; i < N; i++) wait(NULL);

	return 0;
}
