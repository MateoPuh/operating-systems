#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int M;
int A;

//Thread function for increasing variable a
void* thread_func(void* arg)
{
	A += M;
}

int main(int argc, char* argv[])
{
	int N;
	A = 0;

	if (argc == 3) {
		N = atoi(argv[1]);
		M = atoi(argv[2]);
	} else {
		printf( "Error: Wrong number of command line arguments.\n" );
		exit(1);
	}

	pthread_t thr_id[N];

	for (int i = 0; i < N; i++) {
		if (pthread_create(&thr_id[i], NULL, thread_func, NULL) != 0){
			printf( "Error: Cannot create thread.\n" );
			exit(1);
		}

	}

	for (int i = 0; i < N; i++) {
		pthread_join(thr_id[i], NULL);
	}

	printf( "A=%d\n", A);

	return 0;
}
