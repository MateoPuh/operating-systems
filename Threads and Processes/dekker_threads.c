#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int PRAVO = 0;
int ZASTAVICA[2] = {0, 0};

void udi_u_kriticni_odsjecak(i, j)
{
	ZASTAVICA[i] = 1;

	while (ZASTAVICA[j] != 0) {
		if (PRAVO == j) {
			ZASTAVICA[i] = 0;

			while (PRAVO == j);
			ZASTAVICA[i] = 1;

		}
	}
}

void izadi_iz_kriticnog_odsjecka(i, j)
{
	PRAVO = j;
	ZASTAVICA[i] = 0;
}

void ispisi(int i, int k, int m)
{
	printf( "Dretva: %d, K.O. br: %d (%d/5)\n", i, k, m);
}

void* thread_func(void* arg)
{
	int i = *((int *)arg);
	int j = 1 - i;

	for (int k = 1; k <= 5; k++) {
		udi_u_kriticni_odsjecak(i, j);

		for (int m = 1; m <= 5; m++) {
			ispisi(i, k, m);
			sleep(1);
		}

		izadi_iz_kriticnog_odsjecka(i, j);
		sleep(1);
	}
}

int main()
{
	int i = 0, j = 1;

	pthread_t thr_id[2];

	if (pthread_create(&thr_id[0], NULL, thread_func, &i) != 0) {
		printf("Error: Cannot create thread.\n" );
		exit(1);
	}

	if (pthread_create(&thr_id[1], NULL, thread_func, &j) != 0) {
		printf( "Error: Cannot create thread.\n" );
		exit(1);
	}

	pthread_join(thr_id[0], NULL);
	pthread_join(thr_id[1], NULL);

	return 0;
}
