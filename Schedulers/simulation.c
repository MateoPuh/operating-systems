#include <stdio.h>
#include <stdlib.h>

#define MAX_DRETVI 5
#define DRETVI 6

struct dretva {
	int id;
	int p;
	int prio;
	int rasp;
};

struct dretva *P[MAX_DRETVI];

int ispisi_zaglavlje;
int t = 0;
int br_dretvi = 0;
int nove[DRETVI][5] =
{
	{ 1, 3, 5, 3, 1},
	{ 3, 5, 6, 5, 1},
	{ 7, 2, 3, 5, 0},
	{ 12, 1, 5, 3, 0},
	{ 20, 6, 3, 6, 1},
	{ 20, 7, 4, 7, 1},
};

void ispisi_stanja(int ispisi_zagljavlje)
{
	if (ispisi_zagljavlje) {
		printf("  t    AKT");
		for (int i = 1; i < MAX_DRETVI; i++) {
			printf("     PR%d", i);
		}
		printf("\n");
	}
	printf("%3d ", t);
	for (int i = 0; i < MAX_DRETVI; i++) {
		if (P[i] != NULL && P[i]->id != 0) {
			printf("  %d/%d/%d ", P[i]->id,
				P[i]->prio, P[i]->p );
		}
		else {
			printf("  -/-/- ");
		}
	}
	printf("\n");
}

int main()
{
	int zavrsene = 0;
	struct dretva *tmp;
	t = 0;
	ispisi_zaglavlje = 1;

	for (int i = 0; i < MAX_DRETVI; i++) {
		P[i] = NULL;
	}

	ispisi_stanja(ispisi_zaglavlje);
	ispisi_zaglavlje = 0;

	while (zavrsene < DRETVI) {
		//podjela vremena
		if (br_dretvi >= 2) {
			tmp = P[0];
			
			for(int i = 0; i < br_dretvi - 1; i++) {
				P[i] = P[i + 1];
			}

			P[br_dretvi - 1] = tmp;
		}

		ispisi_stanja(0);
		
		if (br_dretvi != 0) {
			P[0]->p--;
			if (P[0]->p == 0) {
				printf("  Dretva %d je zavrsila\n", P[0]->id);
				
				zavrsene++;
				br_dretvi--;

				for(int i = 0; i < br_dretvi ; i++) {
					P[i] = P[i + 1];
				}

				P[br_dretvi] = NULL;
			}
		}


		for (int i = 0; i < DRETVI; i++) {
			if (nove[i][0] == t) {
				tmp = malloc(sizeof(struct dretva));
				tmp->id = nove[i][1];
				tmp->p = nove[i][2];
				tmp->prio = nove[i][3];
				tmp->rasp = nove[i][4];

				P[br_dretvi] = tmp;

				printf("%3d -- nova dretva id=%d, p=%d, prio=%d\n", t,
					 P[br_dretvi]->id, P[br_dretvi]->p, P[br_dretvi]->prio);

				br_dretvi++;
			}
		}

		sleep(1);
		t++;
	}

	return 0;
}


