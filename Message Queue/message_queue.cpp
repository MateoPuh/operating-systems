#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <signal.h>

#define MSG_SIZE 100

int N = 8;
int zavrseni = 0;

int msqid;
long zahtjevtype = 1;
long sjednitype = 2;
long ustanitype = 3;
long zavrsiotype = 4;

struct msgbuf {
    long mtype;
    char mtext[MSG_SIZE];
};

void retreat()
{
    if (msgctl(msqid, IPC_RMID, NULL) == -1) {
        perror("msgctl");
        exit(1);
    }
    exit(0);
}

void retreat(int failure)
{
    retreat();
}

void Posjetitelj(int k) {
    // odradi 3 voznje
    for (int i = 0; i < 3; i++) {

        // spavaj
        int x = rand() % 1900 + 100;
        usleep(x * 1000);

        // priprema poruke zahtjeva za sjedenje
        struct msgbuf buf;
        char mtext[] = "Zelim se voziti";
        memcpy(buf.mtext, mtext, strlen(mtext)+1);
        buf.mtype = zahtjevtype;

        // slanje poruke zahtjeva za sjedenje
        if (msgsnd(msqid, (struct msgbuf *)&buf, strlen(mtext) + 1, 0) == -1) {
            std::cout << "Greska pri slanju poruke zahtjeva." << std::endl;
            retreat();
        }

        // cekaj dozvolu za sjedenje
        struct msgbuf msgr;
        if (msgrcv(msqid, (struct msgbuf *)&msgr, sizeof(msgr)-sizeof(long), sjednitype, 0) == -1) {
            std::cout << "Greska pri primanju poruke sjedenja." << std::endl;
            retreat();
        }

        // sjedni
        std::cout << "Sjeo posjetitelj " << k << std::endl;

        // cekaj poruku za ustajanje
        struct msgbuf msgr2;
        if (msgrcv(msqid, (struct msgbuf *)&msgr2, sizeof(msgr2)-sizeof(long), ustanitype, 0) == -1) {
            std::cout << "Greska pri primanju poruke ustajanja." << std::endl;
            retreat();
        }

        // sidji
        std::cout << "Sisao posjetitelj " << k << std::endl;
    }

    // priprema poruke za zavrsetak
    struct msgbuf msgp;
    char mtext[] = "Posjetitelj zavrsio";
    memcpy(msgp.mtext, mtext, strlen(mtext) + 1);
    msgp.mtype = zavrsiotype;

    // slanje poruke za zavrsetak
    if (msgsnd(msqid, (struct msgbuf *)&msgp, strlen(mtext) + 1, 0) == -1) {
        std::cout << "Greska pri slanju poruke." << std::endl;
        retreat();
    }

    std::cout << "Posjetitelj " << k << " zavrsio" << std::endl;
}

void Vrtuljak() {
    // dok ima posjetitelja
    while(zavrseni < N) {
        int posjetitelji = 0;

        // cekaj paralelno zahtjeve za voznju i poruke za zavrsetak
        while(true) {
            // cekaj zahtjev za voznju
            struct msgbuf msgr;
            if (msgrcv(msqid, (struct msgbuf *)&msgr, sizeof(msgr)-sizeof(long), zahtjevtype, IPC_NOWAIT) != -1) {
                posjetitelji++;

                // priprema poruke za sjedenje
                struct msgbuf msgp;
                char mtext[] = "Sjedni";
                memcpy(msgp.mtext, mtext, strlen(mtext) + 1);
                msgp.mtype = sjednitype;

                // slanje poruke za sjedenje
                if (msgsnd(msqid, (struct msgbuf *)&msgp, strlen(mtext) + 1, 0) == -1) {
                    std::cout << "Greska pri slanju poruke." << std::endl;
                    retreat();
                }

                // kad 4 posjetitelja sjednu porrekni vrtuljak
                if (posjetitelji == 4) break;
            }

            // cekaj poruku za zavrsetak voznje (neblokirajuca)
            struct msgbuf msgrz;
            int msgrcvresz = msgrcv(msqid, (struct msgbuf *)&msgrz, sizeof(msgrz)-sizeof(long), zavrsiotype, IPC_NOWAIT);

            if (msgrcvresz != -1) {
                zavrseni++;
                if (zavrseni == N) break;
            }
        }

        // ako vise nema posjetitelja
        if (zavrseni == N) break;

        // pokretanje vrtuljka
        std::cout << "Pokrenuo vrtuljak" << std::endl;

        int x = rand() % 2000 + 1000;
        usleep(x * 1000);

        // zaustavljanje vrtuljka
        std::cout << "Zaustavio vrtuljak" << std::endl;

        // posalji poruke za ustajanje
        for (int i = 0; i < 4; i++) {
            struct msgbuf msgp;
            char mtext[] = "Ustani";
            memcpy(msgp.mtext, mtext, strlen(mtext) + 1);
            msgp.mtype = ustanitype;

            if (msgsnd(msqid, &msgp, sizeof(msgp.mtext), 0) == -1) {
                std::cout << "Greska pri slanju poruke." << std::endl;
                retreat();
            }
        }
    }
}

int main() {
    msqid = msgget(getuid(), 0600 | IPC_CREAT);

    if (msqid == -1) {
        std::cout << "Greska pri stvaranju reda." << std::endl;
        retreat();
    }

    sigset(SIGINT, retreat);

    int i;

    // stvori N posjetitelja
    for (i = 0; i < N; i++)
        switch (fork()) {
            case 0:
                Posjetitelj(i);
                exit(0);
            case -1:
                std::cout << "Greska pri stvaranju procesa" << std::endl;
                retreat();
    }

    Vrtuljak();

    msgctl(msqid, IPC_RMID, NULL);

    while (i--) wait (NULL);

    return 0;
}
