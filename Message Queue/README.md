# Assignment: Message Queue

## Task 1: Message Queue (`message_queue.c`)

Model Carousel with 2 types of processes: `Visitor` and `Carousel`. Visitor can enter Carousel when it is not full (max 4 visitors) and when all previous visitors left Carousel. Carousel can run only when it is full.
Main thread creates 8 Visitor processes and 1 Carousel. Processes communicate using Message Queue. Processes are syncronized ssing a distributed centralized protocol where the Carousel process is the node that is responsible for mutual exclusion. 
