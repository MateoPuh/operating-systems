#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <signal.h>
#include <vector>

#define N 5
#define ZAHTJEV 1
#define ODGOVOR 2

struct msgbuf {
    int type;
    int i;
    int t;
};

void change_ndelay(int pfd, bool delay) {
    unsigned int open_flag = fcntl(pfd, F_GETFL);

    if (delay) {
        if (fcntl(pfd, F_SETFL, open_flag | O_NDELAY) == -1) {
            printf("failed to set flag\n");
            exit(0);
        }
    } else {
        if (fcntl(pfd, F_SETFL, open_flag & ~O_NDELAY) == -1) {
            printf("failed to set flag\n");
            exit(0);
        }
    }
}

void posalji_odgovor(int i, int t, int pfd, int j) {
    printf("Filozof %d: saljem odgovor(%d, %d) procesu %d.\n", i, i, t, j);

    struct msgbuf bufr;
    bufr.type = ODGOVOR;
    bufr.i = i;
    bufr.t = t;
    (void) write(pfd, &bufr, sizeof(bufr));
}

bool obradi_zahtjev(int i, int ci, msgbuf buf, int pfd, bool salji) {
    // ako je zahtjev stigao prije posalji odgovor
    if (ci > buf.t || ((ci == buf.t) && i > buf.i) || salji) {
        posalji_odgovor(i, buf.t, pfd, buf.i);
        return true;
    }

    return false;
}

int azuriraj_vrijeme(int ci, int t) {
    return std::max(ci, t) + 1;
}

void posalji_zahtjev(int i, int t, int pfd, int j) {
    printf("Filozof %d: saljem zahtjev(%d, %d) procesu %d.\n", i, i, t, j);

    struct msgbuf bufr;
    bufr.type = ZAHTJEV;
    bufr.i = i;
    bufr.t = t;
    (void) write(pfd, &bufr, sizeof(bufr));
}

void filozof(int i) {
    int pfd[N];
    int ci = 0;
    struct msgbuf bufw;
    struct msgbuf bufr;
    std::vector<struct msgbuf> neobradjeni;
    int zahtjevi = 0;

    // otvori cjevovode - svoj za citanje, a tudje za pisanje
    for(int j = 0; j < N; j++) {
        char name[] = "./cjevi";
        name[6] = '0' + j;

        if (i == j) {
            pfd[j] = open(name, O_RDONLY | O_NDELAY);
            continue;
        }

        pfd[j] = open(name, O_WRONLY);
    }

    // sudjeluj na konferenciji
    int x = rand() % 1900 + 100;
    x += i * 1000;

    usleep(x * 1000);

    // procitaj sve poruke
    while (read(pfd[i], &bufr, sizeof(bufr)) != -1) {
        printf("Filozof %d: primio zahtjev(%d, %d)\n", i, bufr.i, bufr.t);

        // ako ne obradi zahtjev onda ga dodaj u listu neobradjenih
        if (!obradi_zahtjev(i, ci, bufr, pfd[bufr.i], false)) {
            neobradjeni.push_back(bufr);
        } else {
            zahtjevi++;
        }

        ci = azuriraj_vrijeme(ci, bufr.t);
    }

    // neka pozivi ubuduce budu blokirajuci
    change_ndelay(pfd[i], true);

    // posalji svima zahtjev za ulazak
    for(int j = 0; j < N; j++) {
        if (i == j) continue;

        posalji_zahtjev(i, ci, pfd[j], j);
    }

    int odgovori = 0;

    change_ndelay(pfd[i], false);

    // primaj poruke dok ne dobijes N odgovora
    while(true) {
        int s = read(pfd[i], &bufr, sizeof(bufr));
        if (s == -1) continue;

        // ako si primio odgovor povecaj brojac
        if (bufr.type == ODGOVOR) {
            odgovori++;
        } else {
            // ako si primio zahtjev obradi ga ili spremi u listu neobradjenih
            printf("Filozof %d: primio zahtjev(%d, %d)\n", i, bufr.i, bufr.t);

            if(!obradi_zahtjev(i, ci, bufr, pfd[bufr.i], false)) {
                neobradjeni.push_back(bufr);
            } else {
                zahtjevi++;
            }

            ci = azuriraj_vrijeme(ci, bufr.t);
        }

        // ako si primio N - 1 odgovora izadji
        if (odgovori == N - 1) break;
    }

    // jedi
    printf("Filozof %d je za stolom.\n", i);
    usleep(3 * 1000 * 1000);

    // obradi sve neobradjene odgovore
    while(zahtjevi != N - 1) {
        std::vector<struct msgbuf> neobradjeni2;

        // obradi sve neobradjene
        while (!neobradjeni.empty()) {
            msgbuf m = neobradjeni.back();
            if (!obradi_zahtjev(i, ci, m, pfd[m.i], true)) {
                neobradjeni2.push_back(m);
            } else {
                zahtjevi++;
            }
            neobradjeni.pop_back();
        }

        neobradjeni = neobradjeni2;

        // cekaj jos zahtjeva dok ne primis N - 1 zahtjeva
        int s = read(pfd[i], &bufr, sizeof(bufr));
        if (s == -1) continue;

        if(!obradi_zahtjev(i, ci, bufr, pfd[bufr.i], false)) {
            neobradjeni.push_back(bufr);
        } else {
            zahtjevi++;
        }

        ci = azuriraj_vrijeme(ci, bufr.t);
    }
}

int main(void)
{
    // stvori N cjevovoda
    // proces cita iz "svog" cjevovoda a pise u tudje
    for (int i = 0; i < N; i++) {
        char name[] = "./cjevi";
        name[6] = '0' + i;
        unlink(name);

        if (mknod(name, S_IFIFO | 00600, 0)==-1)
            exit(1);
    }

    for (int i = 0; i < N; i++) {
        switch (fork()) {
            case -1:
                exit(1);

            case 0:
                filozof(i);
                exit(0);
        }
    }

    for (int i = 0; i < N; i++) {
        wait(NULL);
    }

    return 0;
}