# Assignment: Pipeline

## Task 1: Pipeline (`pipeline.c`)

There are N `Philosophers` and 1 table. There is room for only 1 Philosopher on the table (so, access to the table is critical section). Main thread creates N Philosophers (N is from interval [3, 10]). Philosophers communicate using `Pipeline`. Processes are synchronized using Ricart-Agrawal protocol.
