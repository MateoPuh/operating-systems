#include<math.h>
#include<stdlib.h>
#include<stdio.h>
#include<signal.h>
#include<unistd.h>
#include<sys/time.h>

int pauza = 0;
unsigned long int broj = 1000000001;
unsigned long int zadnji = 1000000001;

void periodicki_ispis()
{
	printf( "Zadnji prosti broj = %lu\n", zadnji );
}

void postavi_pauzu()
{
	pauza = 1-pauza;
}

void prekini( int sig )
{
	periodicki_ispis();
	exit(1);
}

int prost( unsigned long n )
{
	unsigned long i, max;

	if( ( n&1 ) == 0 ){
		return 0;
	}

	max = ( int )sqrt ( n );

	for( i = 3; i <= max; i += 2){

		if( ( n%i ) == 0){
			return 0;
		}

	}

	return 1;
}

int main()
{
	struct itimerval t;

	sigset( SIGINT, postavi_pauzu );
	sigset( SIGTERM, prekini );
	sigset( SIGALRM, periodicki_ispis );

	t.it_value.tv_sec = 0;
	t.it_value.tv_usec = 500000;
	t.it_interval.tv_sec = 0;
	t.it_interval.tv_usec = 500000;
	setitimer( ITIMER_REAL, &t, NULL );

	while(1){

		if( prost(broj) ){
			zadnji = broj;
		}

		broj++;

		while( pauza == 1 ){
			pause();
		}

	}

	return 0;
}
