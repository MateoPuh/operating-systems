#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

int pid = 0;
int sig[] = { SIGUSR1, SIGUSR2, SIGTSTP, SIGQUIT };

void prekidna_rutina( int sig )
{
	kill( pid, SIGKILL );
	exit(0);
}

int main( int argc, char *argv[] )
{
	int randSig;
	int sek;

	pid = atoi( argv[1] );
	sigset( SIGINT, prekidna_rutina );

	srand( (unsigned) time(NULL) );

	while(1){
		sek = rand()%3+3;
		randSig = rand()%4;

		sleep( sek );
		kill( pid, sig[randSig] );
	}

	return 0;
}
