#include<stdio.h>
#include<signal.h>

#define N 6

int OZNAKA_CEKANJA[N];
int PRIORITET[N];
int TEKUCI_PRIORITET;

int sig[] = { SIGUSR1, SIGUSR2, SIGTSTP, SIGQUIT, SIGINT };

void zabrani_prekidanje()
{
	int i;

	for( i=0; i<5; i++ ){
		sighold( sig[i] );
	}
}

void dozvoli_prekidanje()
{
	int i;

	for( i=0; i<5; i++ ){
		sigrelse( sig[i] );
	}
}

void obrada_signala( int i )
{
	char poruka[] = " -  -  -  -  -  -";
	poruka[i+i*2+1] = 'P';

	printf( "%s",poruka );
	ispisi_oznake_cekanja();
	ispisi_tekuci_prioritet();
	ispisi_prioritete();

	for( int j=0; j<5; j++ ){
		sleep(1);
		poruka[i+i*2+1] = j+49;

		printf( "%s",poruka );
		ispisi_oznake_cekanja();
		ispisi_tekuci_prioritet();
		ispisi_prioritete();
	}

	poruka[i+i*2+1] = 'K';
	printf( "%s",poruka );
	ispisi_oznake_cekanja();
	ispisi_tekuci_prioritet();
	ispisi_prioritete();
}

void prekidna_rutina( int sig )
{
	int x;
	int n = 0;

	zabrani_prekidanje();

	switch( sig ){
		case SIGUSR1:
			n = 1;
			printf( " -  X  -  -  -  -" );
			break;
		case SIGUSR2:
			n = 2;
			printf( " -  -  X  -  -  -" );
			break;
		case SIGTSTP:
			n = 3;
			printf( " -  -  -  X  -  -" );
			break;
		case SIGQUIT:
			n = 4;
			printf( " -  -  -  -  X  -" );
			break;
		case SIGINT:
			n = 5;
			printf( " -  -  -  -  -  X" );
			break;
		default:
			break;
	}

	ispisi_oznake_cekanja();
	ispisi_tekuci_prioritet();
	ispisi_prioritete();

	OZNAKA_CEKANJA[n]++;

	do{
		x = 0;
		for( int j=TEKUCI_PRIORITET + 1; j <= N; j++ ){
			if( OZNAKA_CEKANJA[j] > 0 ){
				x = j;
			}
		}

		if( x > 0 ){
			OZNAKA_CEKANJA[x]--;
			PRIORITET[x] = TEKUCI_PRIORITET;
			TEKUCI_PRIORITET = x;

			dozvoli_prekidanje();
			obrada_signala( x );
			zabrani_prekidanje();

			TEKUCI_PRIORITET = PRIORITET[x];
			if( OZNAKA_CEKANJA[x] == 0 ){
				PRIORITET[x] = 0;
			}
		}

	}while( x > 0 );

	dozvoli_prekidanje();
}

void ispisi_oznake_cekanja()
{
	printf( "      O_CEK[%d %d %d %d %d %d]" ,
		OZNAKA_CEKANJA[0], OZNAKA_CEKANJA[1],
		OZNAKA_CEKANJA[2], OZNAKA_CEKANJA[3],
		OZNAKA_CEKANJA[4], OZNAKA_CEKANJA[5]
	);
}

void ispisi_tekuci_prioritet()
{
	printf( "   TEK_PRIOR=%d" ,TEKUCI_PRIORITET );
}

void ispisi_prioritete()
{
	printf( "   PRIOR[%d %d %d %d %d %d]\n" ,
		PRIORITET[0], PRIORITET[1], PRIORITET[2],
		PRIORITET[3], PRIORITET[4], PRIORITET[5]
	);
}

int main( void)
{
	int i = 1;

	sigset( SIGUSR1, prekidna_rutina );
	sigset( SIGUSR2, prekidna_rutina );
	sigset( SIGTSTP, prekidna_rutina );
	sigset( SIGQUIT, prekidna_rutina );
	sigset( SIGINT, prekidna_rutina );

	printf( "Proces obrade prekida, PID=%ld\n", getpid() );
	printf( "GP S1 S2 S3 S4 S5\n" );
	printf( "------------------\n" );

	while( i < 20 ){
		sleep(1);
		printf( "%2d  -  -  -  -  -" ,i );
		ispisi_oznake_cekanja();
		ispisi_tekuci_prioritet();
		ispisi_prioritete();
		i++;
	}

	printf( "Zavrsio osnovni program\n" );

	return 0;

}
