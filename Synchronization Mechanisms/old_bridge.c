#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>

pthread_mutex_t M;
pthread_cond_t red[2];

int smjerNaMostu = 0;
int brAutaNaMostu = 0;

void popniSeNaMost(int smjer)
{
	pthread_mutex_lock(&M);

	while (((smjer != smjerNaMostu) && (brAutaNaMostu > 0)) || brAutaNaMostu >= 3) {
		pthread_cond_wait(&red[smjer], &M);
	}

	brAutaNaMostu++ ;
	smjerNaMostu = smjer;

	pthread_mutex_unlock(&M);
}

void sidiSaMosta (int smjer)
{
	pthread_mutex_lock(&M);

	brAutaNaMostu--;

	if (brAutaNaMostu == 0) {
		pthread_cond_signal(&red[1-smjer]);
		pthread_cond_signal(&red[1-smjer]);
		pthread_cond_signal(&red[1-smjer]);
	} else {
		pthread_cond_signal(&red[smjer]);
	}

	pthread_mutex_unlock(&M);
}

void* automobil (void* broj)
{
	int br = *((int*) broj);
	int smjerAuta;

	srand((unsigned) time(NULL));
	smjerAuta = rand() % 2;

	printf("Auto %d ceka na prijelaz mosta u smjeru %d\n", br, smjerAuta);

	popniSeNaMost(smjerAuta);

	printf("Auto %d prijelazi most\n", br);

	sleep(rand()%5 + 1);

	sidiSaMosta(smjerAuta);

	printf("Auto %d je sisao s mosta\n", br);
}

int main()
{
	pthread_t thr_id[10];

	pthread_mutex_init(&M, NULL);
	pthread_cond_init(&red[0], NULL);
	pthread_cond_init(&red[1], NULL);

	srand((unsigned) time(NULL));

	for (int i = 0; i < 10; i++) {
		pthread_create(&thr_id[i], NULL, automobil, &i);
		sleep(1);
	}

	for (int i = 0; i < 10; i++) {
		pthread_join(thr_id[i], NULL);
	}

	return 0;
}
