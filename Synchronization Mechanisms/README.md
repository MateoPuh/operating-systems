# Assignment: Synchronization Mechanisms

## Task 1: Semaphores (`smokers.c`)

Solve smokers problem using `Binary Semaphores`. Problem consists of 2 types of threads: `Smoker` and `Dealer`. There are 3 Smoker threads, each with different unlimited supply of tobacco product needed to make and smoke a cigarette: `Tobacco`, `Rolling Paper` and `Matches`. Dealer owns all three product in unlimited amount. Dealer randomly selects 2 out of 3 tobacco products, puts them on table and notifies Smokers. Only Smoker who has third product can take those 2 products from table and smoke a cigarette. The process is then repeated indefinitely.

## Task 2: Monitors (`old_bridge.c`)

Solve old bridge problem using `Monitors`. There should be at most 3 cars on the bridge, all going in the same direction. If the bridge is empty and a car is waiting then that car should go on the bridge. Whenever bridge is not empty nor full and cars on the bridge are going in the same direction as a car that is waiting then that car should go on the bridge.
