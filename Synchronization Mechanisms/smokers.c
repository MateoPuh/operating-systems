#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>
#include <semaphore.h>

sem_t stolPrazan, stolPun;
int naStolu;
char *sastojci[3] = { "papir", "duhan", "sibice" };

void *pusac(void *arg)
{
	int i = *((int*) arg);

	printf( "Pusac %d: ima %s\n", i, sastojci[i - 1]);

	while (1) {
		sem_wait(&stolPun);

		if (naStolu == i) {

			printf( "Pusac %d uzima sastojke i pusi\n", i);

			sem_post(&stolPrazan);

		} else {

			sem_post(&stolPun);

			sleep(2);

		}
	}
}

void *trgovac()
{

	srand((unsigned) time(NULL));

	while (1) {

		sem_wait(&stolPrazan);

		naStolu = rand() % 3 + 1;

		if (naStolu == 1) {
			printf("Trgovac stavlja duhan i sibice\n");
		} else if (naStolu == 2) {
			printf("Trgovac stavlja papir i sibice\n");
		} else if (naStolu == 3) {
			printf("Trgovac stavlja papir i duhan\n");
		} else {
			printf("greska");
		}

		sem_post(&stolPun);

		sleep(4);
	}
}

int main ()
{
	pthread_t thr_id[4];

	sem_init(&stolPrazan, 0, 1);
	sem_init(&stolPun, 0, 0);

	for (int i = 1; i < 4; i++) {
		pthread_create(&thr_id[i], NULL, pusac, &i);
		sleep(1);
	}

	sleep (3);

	pthread_create(&thr_id[0], NULL, trgovac, NULL);

	for (int i = 0; i < 4; i++) {
		pthread_join(thr_id[i], NULL);
	}

	sem_destroy(&stolPrazan);
	sem_destroy(&stolPun);

	return 0;
}
