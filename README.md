# Operating Systems

Assigments made as part of `Operating Systems` course (Academic year 2017./18.) on FER:

- `Signals`
- `Threads and Processes`
- `Schedulers`
- `Synchronization Mechanisms`

Assigments made as part of `Advanced Operating Systems` course (Academic year 2019./20.) on FER:

- `Message Queue`
- `Pipeline`
